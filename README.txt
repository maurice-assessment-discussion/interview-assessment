Hello,

For this solution, I used an object wrapper after drilling down the the stage group level, all of
the key/value pairs under a stage group where in the format of python mapping.  {'data': 'dataval', 'data2': 'dataval2'}.

This allowed me to access the values easily:
    obj = DictTypeRapper(mapping_object)
    obj.pm
    obj.ux
    obj.tech_writer

Then I can loop through the list of role types which I stored as a constant in the beginning of the script.  I also have a
file named 'gl_dept_group_only_values' that contains the group names for the various departments.

As the instructions stated, we are only to focus on the engineering team, however the way that my script is structured, is
that once it has drilled down to the print() level *(just after the second try/except), it already has values for
stage and group that can be used to determine which department group from gl_dept_group matches up with the stage and stage
group that is being looped over.  As the instructions stated, the script should be able to handle other departments outside
of engineering, even though engineering was the only focus.

You will see I got the group name by using => gl_dept_group = "eng-dev-{}-{}".format(k, stage_group)

This could easily be removed to get other yaml values from to automate the eng-dev part.  I should also note, that if
the roles for each member/manager was desired, that could have been accomplished by storing the subtype (or group role)
along with the group member/manager as a dictionary pair while it was being looped over:

for subtype in GROUPS:
    {subtype: obj.__getattr__(subtype)}


For the single-engineer group I only grabbed the PM role