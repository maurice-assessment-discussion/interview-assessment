import yaml
from collections import abc

"""
Comments are removed in this solution because its the same, except it pulls in team.yml

"""
GROUPS = ['pm', 'pmm', 'cm', 'backend_engineering_manager', 'frontend_engineering_manager', 'support', 'pdm', 'sets',
          'ux', 'uxr', 'tech_writer', 'tw_backup', 'appsec_engineer']


class DictTypeRapper(object):
    def __new__(cls, cmd):
        if isinstance(cmd, abc.Mapping):
            return super().__new__(cls)
        elif isinstance(cmd, abc.MutableSequence):
            return [cls(item) for item in cmd]
        else:
            return cmd

    def __init__(self, mapping):
        self._data = {}
        for k, v in mapping.items():
            self._data[k] = v

    def __getattr__(self, name):
        if hasattr(self._data, name):
            return getattr(self._data, name)
        else:
            return DictTypeRapper(self._data[name])


class ParseYAML(object):

    @classmethod
    def parsevalues(cls, y_file, preset=None):
        with open(y_file, 'r') as f:
            try:
                if preset is None:
                    return yaml.safe_load(f)
                return yaml.safe_load(f)[preset]

            except yaml.YAMLError as err:
                print(err)


with open('gl_dept_group_only_values') as fstream:
    department_group_names = []
    for line in fstream.readlines():
        department_group_names.append(line.strip())


def process_engineering_groups():
    organization_groupings = []
    single_engineer_groups = []
    # Parse the stages.yml file to get the data
    stages = ParseYAML.parsevalues('stages.yml', 'stages')

    # loop through the stages
    for k, v in stages.items():

        if k not in ['deploy', 'mobile', 'learn']:

            # loop through the groups under each stage
            for stage_group, vals in v['groups'].items():

                members = []
                managers = []
                obj = DictTypeRapper(vals)
                try:
                    # Backend and Frontend Managers are added to the managers array
                    managers.append(obj.__getattr__('backend_engineering_manager'))
                    managers.append(obj.__getattr__('frontend_engineering_manager'))

                except KeyError as err:  # likely no frontend and/or backend engineer present
                    pass

                # loop through the constant GROUPS and use __getattr__() to access the obj attr that matches the string
                try:
                    for subtype in GROUPS:
                        if '_engineering_manager' in subtype:
                            continue
                        try:
                            members.append(obj.__getattr__(subtype))
                        except KeyError as err:  # Errors here means the key does not exists
                            continue
                except:  # let's just move on!
                    pass

                gl_dept_group = "eng-dev-{}-{}".format(k, stage_group)
                organization_groupings.append([gl_dept_group, managers, members])

        # This first part is for the single-engineer groups
        obj = DictTypeRapper(v)
        # going to stick with pm only, per the instructions
        try:
            for _grp, _val in v['groups'].items():
                gl_dept_group = "eng-dev-{}-{}".format(k, _grp)
            organization_groupings.append([gl_dept_group, obj.pm, []])
        except KeyError as err:
            continue

    return organization_groupings

# only using threading to load the yaml and execute process_engineering_groups() at the same time because
# team.yml is large, it seems better to do it this way instead of waiting for the yaml to load.


def load_handles():
    return ParseYAML.parsevalues('team.yml')


if __name__ == "__main__":
    d = process_engineering_groups()
    t = load_handles()
    slug_objects = {}
    new_data = ""
    for mem in t:
        slug = DictTypeRapper(mem)
        try:
            slug_objects[slug.name] = slug.gitlab
        except:
            pass

    print(d)